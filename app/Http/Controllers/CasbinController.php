<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Enforcer;

class CasbinController extends Controller
{
    public function index()
    {
        # RBAC Model
        // adds permissions to a role
        Enforcer::addPolicy('admin', 'server','edit');

        Enforcer::addPolicy('pro-customer', 'server','edit');
        Enforcer::addPolicy('pro-customer', 'server:webapp','edit');
        Enforcer::addPolicy('pro-customer', 'server:atomic-deployment','edit');

        // assign a role for a user.
        Enforcer::addRoleForUser('user1', 'pro-customer');

        // eve have role admin
        Enforcer::enforce('eve', 'server', 'edit'); // true because admin has edit permission
        Enforcer::enforce('eve', 'server', 'read'); // false because we didnt specify read permission to admin

        # RBAC with Domain Model
        // adds permissions to a role on domain
        Enforcer::guard('team')->addPolicy('admin', 'team1','server','*', 'server');

        // kene get owner user_id
        // check dia owner ke tak by user_id
        // lepas

        // assign a role for a user on a domain
        Enforcer::guard('team')->addGroupingPolicy('alice', 'admin', 'team1');

        // alice have role admin on team1
        Enforcer::guard('team')->enforce('alice', 'team1', 'server', 'read', 'server'); // true because admin on team1 has read role
        Enforcer::guard('team')->enforce('alice', 'team2', 'server', 'read', 'server'); // false because we didnt give admin role on team2 to alice
        Enforcer::guard('team')->enforce('alice', 'team1', 'server', 'edit', 'server'); // false because we didnt give edit permission to admin on team1

        // Enforcer::guard('team')->enforce('user_id', 'team_id', 'resource', 'permission');
    }

    public function basic()
    {
        // adds permissions to a user
        Enforcer::addPermissionForUser('eve', 'articles', 'read');
        // adds a role for a user.
        Enforcer::addRoleForUser('eve', 'writer');
        // adds permissions to a rule
        Enforcer::addPolicy('writer', 'articles','edit');
    }

    public function usage()
    {
        $IAM->addRole('admin')->toTeam($team_id)->toResource('server')->forService('server');
        $IAM->setUser($user_id)->addToTeam($team_id)->assignRole('admin');

    }
}
